#include <aspire/WriteFileQML.h>

#include <aspire/FactoryComponent.h>
#include <private/qqmlmetatype_p.h>
#include <fstream>
#include <unordered_set>

using aspire::FactoryComponent;

namespace
{
	auto IsProperty(const QObject& parent, const QObject& child) -> bool
	{
		const auto* metaObject = parent.metaObject();

		for(auto i = 0; i < metaObject->propertyCount(); i++)
		{
			const auto property = metaObject->property(i);
			const auto variant = property.read(&parent);

			if(variant.canConvert<QObject*>() == false)
			{
				continue;
			}

			if(variant.value<QObject*>() != &child)
			{
				continue;
			}

			return true;
		}

		return false;
	}

	auto WriteProperties(QObject& x, std::string& tabs, std::ofstream& ofs) -> void
	{
		const auto* metaObject = x.metaObject();

		if(metaObject == nullptr)
		{
			return;
		}

		auto* component = FactoryComponent::Instance()->findComponent(&x);

		if(component == nullptr)
		{
			// Unregistered type?
			return;
		}

		auto defaultObject = std::unique_ptr<QObject>(component->create());

		if(defaultObject == nullptr)
		{
			// Something has gone wrong.
			return;
		}

		tabs.push_back('\t');

		// Store available enums for the object type in a set to perform
		// O(1) search to determine if the object's enumeration is a global
		// enumeration or local enumeration for the object.
		std::unordered_set<std::string> enumCache;

		for(auto i = 0; i < metaObject->enumeratorCount(); i++)
		{
			auto metaEnum = metaObject->enumerator(i);
			enumCache.insert(metaEnum.name());
		}

		for(auto i = 0; i < metaObject->propertyCount(); i++)
		{
			const auto property = metaObject->property(i);
			const auto variant = property.read(&x);
			std::string value;

			const auto defaultValue = property.read(defaultObject.get());

			// Skip values that have not changed from their default value.
			if(defaultValue == variant)
			{
				continue;
			}

			if(property.isEnumType() == true)
			{
				auto metaEnum = property.enumerator();

				if(enumCache.find(metaEnum.name()) != std::end(enumCache))
				{
					value += FactoryComponent::Instance()->findQmlName(metaEnum.enclosingMetaObject());

					if(metaEnum.isScoped() == true)
					{
						value += ".";
						value += metaEnum.name();
					}
				}
				else
				{
					value += metaEnum.enclosingMetaObject()->className();
				}

				value += ".";
				value += metaEnum.valueToKey(variant.toInt());
			}
			else if(property.userType() == QMetaType::QColor)
			{
				value = "\"" + variant.toString().toStdString() + "\"";
			}
			else
			{
				value = variant.toString().toStdString();
			}

			if(std::empty(value) == true)
			{
				continue;
			}

			ofs << tabs << property.name() << ": " << value << "\n";
		}

		tabs.pop_back();
	}

	auto WriteObject(QObject& x, std::string& tabs, std::ofstream& ofs) -> void
	{
		const auto elementName = FactoryComponent::Instance()->findQmlName(x.metaObject());

		if(std::empty(elementName) == true)
		{
			return;
		}

		ofs << tabs << elementName << " {\n";

		WriteProperties(x, tabs, ofs);

		tabs.push_back('\t');

		for(auto* child : x.children())
		{
			// If the child is a property of the object, then skip as it will be handled
			// via WriteProperties.
			if(IsProperty(x, *child) == true)
			{
				continue;
			}

			ofs << "\n";
			WriteObject(*child, tabs, ofs);
		}

		tabs.pop_back();

		ofs << tabs << "}\n";
	}
}

auto aspire::WriteFileQML(QObject* x, const std::filesystem::path& file) -> void
{
	if(x == nullptr)
	{
		return;
	}

	std::ofstream ofs{file};

	if(ofs.is_open() == false)
	{
		return;
	}

	ofs << "import QtQuick\n";
	ofs << "\n";

	std::string tabs;
	WriteObject(*x, tabs, ofs);

	ofs << "\n";
}
